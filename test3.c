#include<stdio.h>
#include<math.h>
#include<stdlib.h>
int main()
{
	int a,b,c;
	float root1,root2,D;
	printf ("Enter the coefficients of the variable in the quadratic equation:\n");
	scanf ("%d%d%d",&a,&b,&c);
	D=(b*b)-(4*a*c);
	if (D>0)
	{
		printf ("Roots are real and distinct\n");
		root1=(-b+sqrt(D))/2*a;
		root2=(-b-sqrt(D))/2*a;
	}
	else if (D<0)
	{
		printf ("Roots are imaginary\n");
		root1=(-b+sqrt(fabs(D)))/2*a;
		root2=(-b-sqrt(fabs(D)))/2*a;
	}
	else
	{
		printf ("Roots are real and equal\n");
		root1=(-b+sqrt(D))/2*a;
		root2=(-b-sqrt(D))/2*a;
	}
	printf ("1st root is %f and 2nd root is %f",root1,root2);
	return 0;
}